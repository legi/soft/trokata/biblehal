# Bib le Hal - Synchronisation Bibliographique avec HAL

**Bib le Hal** est un ensemble de script en ```bash``` et en ```perl``` permettant de synchroniser un sous ensemble de la base de donnée bibliographique **HAL** (http://hal.archives-ouvertes.fr/) avec une base locale **Refbase** (http://www.refbase.net).

L'intérêt premier est de pouvoir intégrer dans le site d'un laboratoire de recherche utilisant le moteur **SPIP** (http://www.spip.net/) les références de la base ```HAL``` sans avoir à refaire la moindre saisie. Il est aussi possible d'utiliser la base ```Refbase``` pour faire un peu ce que l'on souhaite...

## Repository

L'ensemble du code est sous **licence libre**.
Les scripts en ```bash``` sont sous GPL version 3 ou plus récente (http://www.gnu.org/licenses/gpl.html),
les scripts en ```perl``` sont sous la même licence que ```perl```
c'est à dire la double licence GPL et Artistic Licence (http://dev.perl.org/licenses/artistic.html).

Les sources sont gérés via subversion (http://subversion.tigris.org/).
Il est très facile de rester synchronisé par rapport à ces sources.

 * la récupération initiale
```bash
git clone https://gricad-gitlab.univ-grenoble-alpes.fr/legi/soft/trokata/biblehal
```
 * les mises à jour par la suite
```bash
git pull
```

Il est possible d'avoir un accès en écriture à la forge
sur demande motivée à [Gabriel Moreau](mailto:Gabriel.Moreau__AT__legi.grenoble-inp.fr).
Pour des questions de temps d'administration et de sécurité,
la forge n'est pas accessible en écriture sans autorisation.
