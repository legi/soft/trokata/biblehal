DESTDIR=

LIBDIR=/usr/lib/biblehal
CRONDIR=/etc/cron.d
ETCDIR=/etc/biblehal

.PHONY: all install update sync help

all: help

install: update
	@id bib-le-hal > /dev/null || adduser --system --group --shell /bin/bash --home /var/lib/biblehal bib-le-hal
	@install -d -m 0755 -o root -g root $(DESTDIR)/$(CRONDIR)
	install     -m 0644 -o root -g root biblehal.cron $(DESTDIR)/$(CRONDIR)/klask

update:
	@install -d -m 0755 -o root -g root $(DESTDIR)/$(LIBDIR)
	install     -m 0755 -o root -g root biblehal-clean $(DESTDIR)/$(LIBDIR)
	install     -m 0755 -o root -g root biblehal-cleanall $(DESTDIR)/$(LIBDIR)
	install     -m 0755 -o root -g root biblehal-count $(DESTDIR)/$(LIBDIR)
	install     -m 0755 -o root -g root biblehal-explode $(DESTDIR)/$(LIBDIR)
	install     -m 0755 -o root -g root biblehal-pop $(DESTDIR)/$(LIBDIR)
	install     -m 0755 -o root -g root biblehal-popall $(DESTDIR)/$(LIBDIR)
	install     -m 0755 -o root -g root biblehal-poplast $(DESTDIR)/$(LIBDIR)
	install     -m 0755 -o root -g root biblehal-push $(DESTDIR)/$(LIBDIR)
	install     -m 0755 -o root -g root biblehal-pushall $(DESTDIR)/$(LIBDIR)

	@install -d -m 0755 -o root -g root $(DESTDIR)/$(ETCDIR)
	install     -m 0644 -o root -g root biblehal-sample.conf $(DESTDIR)/$(ETCDIR)

sync:
	svn update

help:
	@echo "Possible target:"
	@echo " * all     : this help"
	@echo " * install : complete installation"
	@echo " * update  : minimal installation"
	@echo " * sync    : sync with subversion"
